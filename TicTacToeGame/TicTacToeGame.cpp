// TicTacToeGame.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Headers\GameActors\GameGrid.h"
#include "Headers\Utility\StringFormatter.h"
#include "Headers\Utility\HighlightableText.h"
#include "Headers\Utility\BoxParameters.h"
#include <iostream>

HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

int main()
{
	SetConsoleTitleA("Tic Tac Toe");

	GameGrid gameGrid(3);
	std::vector<HighlightableText> drawnGrid = gameGrid.PrepareForDrawing();

	for (HighlightableText text : drawnGrid) {
		strFormat::PrintHighlightedString(handle, text);
	}

    return 0;
}

