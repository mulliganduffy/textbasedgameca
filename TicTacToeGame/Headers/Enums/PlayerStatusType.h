#pragma once
#ifndef PLAYER_STATUS_TYPE_H
#define PLAYER_STATUS_TYPE_H

enum class PlayerStatusType {
	Empty,
	PlayerOne,
	PlayerTwo
};

#endif // !PLAYER_STATUS_TYPE_H
