#pragma once
#ifndef BOX_PARAMETERS_H
#define BOX_PARAMETERS_H

struct BoxParameters {
	unsigned short borderColor;
	unsigned short backgroundColor;
	int padding;
	char borderChar;
};

#endif // !BOX_PARAMETERS_H
