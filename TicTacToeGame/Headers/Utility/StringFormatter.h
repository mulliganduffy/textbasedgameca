#pragma once
#ifndef STRING_FORMATTER_H
#define STRING_FORMATTER_H

#include "HighlightableText.h"
#include "BoxParameters.h"
#include <Windows.h>

namespace strFormat {
	//Constants
	const WORD WHITE_TEXT = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
	const BoxParameters DEFAULT_BOX = { WHITE_TEXT, WHITE_TEXT, 1, '*' };

	//Highlights
	void PrintHighlightedString(const HANDLE& handle, const HighlightableText& text, const bool& bNewLine = true);
	WORD GetColorForChar(const std::vector<TextHighlightParameters>& highlightParameters, const size_t& charPosition);

	//Boxes
	void PrintStringInsideBox(const std::string& line, BoxParameters box = DEFAULT_BOX);
	void PrintMultipleStringsInsideBox(const std::vector<std::string>& lines, BoxParameters box = DEFAULT_BOX);
	size_t GetLongestStringInVector(const std::vector<std::string>& lines);
	size_t GetLongestStringInVector(const std::vector<HighlightableText>& lines);

	//Highlights AND Boxes
	void PrintHighlightedStringInsideBox(const HANDLE& handle, const HighlightableText& text, BoxParameters box = DEFAULT_BOX);
	void PrintMultipleHighlightedStringsInsideBox(const HANDLE& handle, const std::vector<HighlightableText>& texts, BoxParameters box = DEFAULT_BOX);
}

#endif // !STRING_FORMATTER_H
