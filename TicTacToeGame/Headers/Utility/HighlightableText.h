#pragma once
#ifndef HIGHLIGHTABLE_TEXT_H
#define HIGHLIGHTABLE_TEXT_H

#include <vector>
#include <string>

struct TextHighlightParameters {
	int startPoint;
	int endPoint;
	unsigned short highlightColor;
};

class HighlightableText {
public:
	HighlightableText(std::string rawString);
	HighlightableText(std::string rawString, int highlightStartPoint, int highlightEndPoint, unsigned short highlightColor);

	std::string GetRawString() const;
	std::vector<TextHighlightParameters> GetHighlightParameters() const;

	void AddNewHighlightParameter(int startPoint, int endPoint, unsigned short highlightColor);

private:
	std::string m_rawString;
	std::vector<TextHighlightParameters> m_highlightParameters;
};

#endif // !HIGHLIGHTABLE_TEXT_H
