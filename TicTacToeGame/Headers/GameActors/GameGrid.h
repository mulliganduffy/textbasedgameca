#pragma once
#ifndef GAME_GRID_H
#define GAME_GRID_H

#include "../Templates/VectorGrid.h"
#include "../Enums/PlayerStatusType.h"
#include "../Utility/HighlightableText.h"

class GameGrid {
public:
	GameGrid(const int& gameSize);

	std::vector<HighlightableText> PrepareForDrawing();
	HighlightableText CreateGridEnds();
	void BuildUpGrid(std::vector<HighlightableText>& preparedGrid);
	void BuildCurrentGridRow(std::vector<HighlightableText>& preparedGrid);

private:
	VectorGrid<PlayerStatusType> m_gameGrid;
	bool m_gameEnded;
};

#endif // !GAME_GRID_H
