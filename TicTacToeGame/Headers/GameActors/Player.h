#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include "../Enums/PlayerStatusType.h"
#include <string>

class Player
{
public:
	Player();
	Player(std::string playerName, PlayerStatusType playerType);

private:
	std::string m_playerName;
	PlayerStatusType m_playerType;
	unsigned int m_gamesWon;
	unsigned int m_gamesLost;
	unsigned int m_gamesDrawn;
};

#endif // !PLAYER_H
