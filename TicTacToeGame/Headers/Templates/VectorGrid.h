#pragma once
#ifndef VECTOR_GRID
#define VECTOR_GRID

#include <vector>

template <class T>
class VectorGrid {
public:
	VectorGrid(const T& defaultValue, const int& size); //Constructs a square grid
	VectorGrid(const T& defaultValue, const int& width, const int& height); //Constructs a rectangular grid

	T GetElementInCoordinate(const int& xPos, const int& yPos) const;
	int GetGridWidth() const;
	int GetGridHeight() const;

	void SetElementInCoordinate(const T& value, const int& xPos, const int& yPos);

private:
	std::vector<std::vector<T>> m_grid;
	int m_gridWidth;
	int m_gridHeight;
};

template<class T>
inline VectorGrid<T>::VectorGrid(const T & defaultValue, const int & size)
	:VectorGrid(defaultValue, size, size)
{
}

template<class T>
inline VectorGrid<T>::VectorGrid(const T & defaultValue, const int & width, const int & height)
	: m_grid(width, std::vector<T>(height, defaultValue))
{
	m_gridWidth = width;
	m_gridHeight = height;
}

template<class T>
inline T VectorGrid<T>::GetElementInCoordinate(const int & xPos, const int & yPos) const
{
	return m_grid[xPos][yPos];
}

template<class T>
inline int VectorGrid<T>::GetGridWidth() const
{
	return m_gridWidth;
}

template<class T>
inline int VectorGrid<T>::GetGridHeight() const
{
	return m_gridHeight;
}

template<class T>
inline void VectorGrid<T>::SetElementInCoordinate(const T & value, const int & xPos, const int & yPos)
{
	m_grid[xPos][yPos] = value;
}

#endif // !VECTOR_GRID
