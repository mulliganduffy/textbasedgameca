#include "..\..\stdafx.h"
#include "..\..\Headers\Utility\HighlightableText.h"

HighlightableText::HighlightableText(std::string rawString)
{
	m_rawString = rawString;
}

HighlightableText::HighlightableText(std::string rawString, int highlightStartPoint, int highlightEndPoint, unsigned short highlightColor)
{
	m_rawString = rawString;
	AddNewHighlightParameter(highlightStartPoint, highlightEndPoint, highlightColor);
}

std::string HighlightableText::GetRawString() const
{
	return m_rawString;
}

std::vector<TextHighlightParameters> HighlightableText::GetHighlightParameters() const
{
	return m_highlightParameters;
}

void HighlightableText::AddNewHighlightParameter(int startPoint, int endPoint, unsigned short highlightColor)
{
	TextHighlightParameters newParameter = { startPoint, endPoint, highlightColor };
	m_highlightParameters.push_back(newParameter);
}
