#include "..\..\stdafx.h"
#include "..\..\Headers\Utility\StringFormatter.h"
#include <iostream>

void strFormat::PrintHighlightedString(const HANDLE & handle, const HighlightableText & text, const bool& bNewLine)
{
	for (size_t i = 0; i < text.GetRawString().length(); i++) {
		WORD colorForChar = GetColorForChar(text.GetHighlightParameters(), i);
		SetConsoleTextAttribute(handle, colorForChar);
		std::cout << text.GetRawString()[i];
	}
	SetConsoleTextAttribute(handle, WHITE_TEXT);
	if (bNewLine) {
		std::cout << '\n';
	}
}

WORD strFormat::GetColorForChar(const std::vector<TextHighlightParameters>& highlightParameters, const size_t & charPosition)
{
	for (TextHighlightParameters parameter : highlightParameters) {
		if (parameter.startPoint <= charPosition && charPosition <= parameter.endPoint) {
			return parameter.highlightColor;
		}
	}
	return WHITE_TEXT;
}

void strFormat::PrintStringInsideBox(const std::string & line, BoxParameters box)
{
	std::vector<std::string> singleLine;
	singleLine.push_back(line);
	PrintMultipleStringsInsideBox(singleLine, box);
}

void strFormat::PrintMultipleStringsInsideBox(const std::vector<std::string>& lines, BoxParameters box)
{
	const int LONGEST_LENGTH = GetLongestStringInVector(lines);
	const int ROWS = 2 + lines.size() + (box.padding * 2);
	const int COLS = 2 + LONGEST_LENGTH + (box.padding * 2);
	int lineIndex = 0;

	//Print out each row
	for (int r = 0; r < ROWS; r++) {
		std::size_t c = 0;
		//Print out each column in this row
		while (c < COLS) {
			bool bOnString = ((box.padding + 1 <= r && r <= box.padding + lines.size()) && (c == box.padding + 1));
			if (bOnString) {
				std::cout << lines[lineIndex];
				c += lines[lineIndex].length();
				lineIndex++;
			}
			else {
				bool bOnBorder = (r == 0 || r == ROWS - 1 || c == 0 || c == COLS - 1);
				char charToPrint = (bOnBorder) ? box.borderChar : ' ';
				std::cout << charToPrint;
				c++;
			}
		}
		std::cout << '\n';
	}
}

size_t strFormat::GetLongestStringInVector(const std::vector<std::string>& lines)
{
	size_t longestLength = 0;
	for (std::string thisLine : lines) {
		longestLength = (thisLine.length() > longestLength) ? thisLine.length() : longestLength;
	}
	return longestLength;
}

size_t strFormat::GetLongestStringInVector(const std::vector<HighlightableText>& lines) {
	size_t longestLength = 0;
	for (HighlightableText thisLine : lines) {
		longestLength = (thisLine.GetRawString().length() > longestLength) ? thisLine.GetRawString().length() : longestLength;
	}
	return longestLength;
}

void strFormat::PrintHighlightedStringInsideBox(const HANDLE & handle, const HighlightableText & text, BoxParameters box)
{
	std::vector<HighlightableText> singleText;
	singleText.push_back(text);

	PrintMultipleHighlightedStringsInsideBox(handle, singleText, box);
}

void strFormat::PrintMultipleHighlightedStringsInsideBox(const HANDLE & handle, const std::vector<HighlightableText>& texts, BoxParameters box)
{
	const int LONGEST_LENGTH = GetLongestStringInVector(texts);
	const int ROWS = 2 + texts.size() + (box.padding * 2);
	const int COLS = 2 + LONGEST_LENGTH + (box.padding * 2);
	int textIndex = 0;

	//Print out each row
	for (int r = 0; r < ROWS; r++) {
		std::size_t c = 0;
		//Print out each column in this row
		while (c < COLS) {
			bool bOnString = ((box.padding + 1 <= r && r <= box.padding + texts.size()) && (c == box.padding + 1));
			if (bOnString) {
				PrintHighlightedString(handle, texts[textIndex], false);
				c += texts[textIndex].GetRawString().length();
				textIndex++;
			} else {
				bool bOnBorder = (r == 0 || r == ROWS - 1 || c == 0 || c == COLS - 1);
				char charToPrint = (bOnBorder) ? box.borderChar : ' ';
				WORD charColor = (bOnBorder) ? box.borderColor : box.backgroundColor;
				SetConsoleTextAttribute(handle, charColor);
				std::cout << charToPrint;
				c++;
			}
		}
		std::cout << '\n';
	}
	SetConsoleTextAttribute(handle, WHITE_TEXT);
}
