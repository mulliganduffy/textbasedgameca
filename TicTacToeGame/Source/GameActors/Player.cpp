#include "../../stdafx.h"
#include "..\..\Headers\GameActors\Player.h"

Player::Player()
	:Player("Noname", PlayerStatusType::Empty)
{
}

Player::Player(std::string playerName, PlayerStatusType playerType)
{
	m_playerName = playerName;
	m_playerType = playerType;
	m_gamesWon = 0;
	m_gamesLost = 0;
	m_gamesDrawn = 0;
}
