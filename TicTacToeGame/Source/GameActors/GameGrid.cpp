#include "..\..\stdafx.h"
#include "..\..\Headers\GameActors\GameGrid.h"

GameGrid::GameGrid(const int & gameSize)
	:m_gameGrid(PlayerStatusType::Empty, gameSize)
{
	m_gameEnded = false;
}

std::vector<HighlightableText> GameGrid::PrepareForDrawing()
{
	std::vector<HighlightableText> preparedGrid;

	//Draw the top of the grid
	preparedGrid.push_back(CreateGridEnds());
	//Draw the middle of the grid
	BuildUpGrid(preparedGrid);
	//Draw the bottom of the grid
	preparedGrid.push_back(CreateGridEnds());

	return preparedGrid;
}

HighlightableText GameGrid::CreateGridEnds()
{
	std::string gridEndRaw("");
	int colBordersToPrint = m_gameGrid.GetGridWidth() - 1;
	int colSpacesToPrint = m_gameGrid.GetGridWidth();

	for (int i = 0; i < colSpacesToPrint; i++) {
		for (int j = 0; j < 5; j++) {
			gridEndRaw.push_back(' ');
		}
		if (colBordersToPrint != 0) {
			gridEndRaw.push_back('#');
			colBordersToPrint--;
		}
	}
	return HighlightableText(gridEndRaw);
}

void GameGrid::BuildUpGrid(std::vector<HighlightableText>& preparedGrid)
{
	for (int i = 0; i < m_gameGrid.GetGridHeight(); i++) {
		BuildCurrentGridRow(preparedGrid);

		if (i != m_gameGrid.GetGridHeight() - 1) {
			int fullGridWidth = (m_gameGrid.GetGridWidth() * 5) + (m_gameGrid.GetGridWidth() - 1);
			std::string gridSeperatorRaw(fullGridWidth, '#');
			HighlightableText gridSeperator(gridSeperatorRaw);
			preparedGrid.push_back(gridSeperator);
		}
	}
}

void GameGrid::BuildCurrentGridRow(std::vector<HighlightableText>& preparedGrid)
{
	int colBordersToPrint = m_gameGrid.GetGridWidth() - 1;
	for (int i = 0; i < 3; i++) {
		std::string currentSubRowRaw("");
		colBordersToPrint = m_gameGrid.GetGridWidth() - 1;

		for (int k = 0; k < 3; k++) {
			for (int k = 0; k < 5; k++) {
				currentSubRowRaw.push_back(' ');
			}
			if (colBordersToPrint != 0) {
				currentSubRowRaw.push_back('#');
				colBordersToPrint--;
			}
		}

		HighlightableText currentSubRow(currentSubRowRaw);
		preparedGrid.push_back(currentSubRow);
	}
}
